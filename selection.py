# -*- coding: utf-8 -*-
"""
Created on Mon Jun 01 16:18:53 2015

@author: bdu01
"""

def check_ltv(pe, significant_level):
    indicator = pe.Variable.str.strip().str.lower().str.startswith("orig_ltv_rto_grp")
    if not indicator.any():
        return True
    #end if
    pe = pe.ix[indicator, ]
    estimate = pe.ix[0, 'Estimate'] < pe.ix[1, 'Estimate'] < pe.ix[2, 'Estimate'] < pe.ix[3, 'Estimate']
    test = (pe.ProbChiSq < significant_level).any()
    return estimate and test
#end def

def check_age(pe, significant_level):
    indicator = pe.Variable.str.strip().str.lower().str.startswith("vehicle_age_grp")
    if not indicator.any():
        return True
    #end if
    pe = pe.ix[indicator, ]
    estimate = True # not sure how to filtering
    test = (pe.ProbChiSq < significant_level).any()
    return estimate and test
#end def
    
def check_joint(pe, significant_level):
    indicator = pe.Variable.str.strip().str.lower().str.startswith("joint_applcnt_ind")
    if not indicator.any():
        return True
    #end if
    pe = pe.ix[indicator, ]
    estimate = pe.ix[0, 'Estimate'] > 0
    test = (pe.ProbChiSq < significant_level).any()
    return estimate and test
#end def
    
def check_term(pe, significant_level):
    indicator = pe.Variable.str.strip().str.lower().str.startswith("term_grp")
    if not indicator.any():
        return True
    #end if
    pe = pe.ix[indicator, ]
    estimate = pe.ix[0, 'Estimate'] < pe.ix[1, 'Estimate'] < 0
    test = (pe.ProbChiSq < significant_level).any()
    return estimate and test
#end def
    
def check_balance(pe, significant_level):
    indicator = pe.Variable.str.strip().str.lower().str.startswith("cur_bal_amt_grp")
    if not indicator.any():
        return True
    #end if
    pe = pe.ix[indicator, ]
    estimate = pe.ix[0, 'Estimate'] < pe.ix[1, 'Estimate'] < pe.ix[2, 'Estimate'] < 0
    test = (pe.ProbChiSq < significant_level).any()
    return estimate and test
#end def
    
def check_hpi(pe, significant_level):
    indicator = pe.Variable.str.strip().str.lower().str.startswith("hpi_hybrid")
    if not indicator.any():
        return True
    #end if
    pe = pe.ix[indicator, ]
    estimate = pe.ix[0, 'Estimate'] < 0
    test = (pe.ProbChiSq < significant_level).any()
    return estimate and test
#end def
    
def check_manheim(pe, significant_level):
    indicator = pe.Variable.str.strip().str.lower().str.startswith("x1")
    if not indicator.any():
        return True
    #end if
    pe = pe.ix[indicator, ]
    estimate = pe.ix[0, 'Estimate'] < 0
    test = (pe.ProbChiSq < significant_level).any()
    return estimate and test
#end def
    
def check_ur(pe, significant_level):
    indicator = pe.Variable.str.strip().str.lower().str.startswith("ur_hybrid")
    if not indicator.any():
        return True
    #end if
    pe = pe.ix[indicator, ]
    estimate = pe.ix[0, 'Estimate'] > 0
    test = (pe.ProbChiSq < significant_level).any()
    return estimate and test
#end def
    
def filter_model(pe, significant_level=0.05):
    check = [
        check_ltv(pe, significant_level),
        check_age(pe, significant_level),
        check_joint(pe, significant_level),
        check_term(pe, significant_level),
        check_balance(pe, significant_level),
        check_hpi(pe, significant_level),
        check_manheim(pe, significant_level),
        check_ur(pe, significant_level)
    ]
    if not all(check):
        return None
    #end if
    # reshpae data frame
    rv = [pe.ix[0, 'model_id']]
    for i in range(pe.shape[0]):
        rv.extend(list(pe.iloc[i, 0:5]))
    #end for
    return rv
#end def
