# -*- coding: utf-8 -*-
"""
Created on Fri May 29 09:58:58 2015

@author: Ben Chuanlong Du
"""

#%%
import pandas as pd
pd.set_option('display.height', 1000)
pd.set_option('display.width', 1000)
pd.set_option('display.max_rows', 100)
pd.set_option('display.max_columns', 50)
pd.set_option('display.max_colwidth', 100)
pd.reset_option("all")
import os
os.chdir("C:\\Users\\bdu01\\Downloads\\archives\\projects\\ia_lgd\\ben_2\\data\\scenarios")
import ccar as ccar
#%% read in the data
file = "raw/DFAST 2015 Scenarios (External) 150415 UPDATE.XLS"
base_sheet = "Base - unlocked"
inflation_sheet = "Inflation - unlocked"
european_sheet = "Euro - unlocked"
skip_rows = range(6) + range(8,17) + range(227, 300)
keep_columns = """
0	5	14	15	16	17	18	19	20	21	22	23	24	25	26	27	28	29	30	31	32	33	34	35	36	37	38	39	40	41	42	43	44	45	46	47	48	49	50	51	52	53	55	57	59	61	63	65	67	69	71	72	74	77	79	82	85	87	89	90	91	92	93	95	97	99	101	102	103	104	107
"""
keep_columns = map(int, keep_columns.split())
#drop_columns = [1, 76] # column names rather than index!!!
base = pd.read_excel(file, base_sheet, header=None, skiprows=skip_rows)
inflation = pd.read_excel(file, inflation_sheet, header=None, skiprows=skip_rows)
european = pd.read_excel(file, european_sheet, header=None, skiprows=skip_rows)
base.head()
base.tail()
#%% fill in missing names due to merged cells
ccar.fill_merged_cells(base, 0)
ccar.fill_merged_cells(inflation, 0)
ccar.fill_merged_cells(european, 0)
#%% drop column 1 and 76 first for comparing column names
drop_columns = [1, 76]
base.drop(drop_columns, axis=1, inplace=True)
inflation.drop(drop_columns, axis=1, inplace=True)
european.drop(drop_columns, axis=1, inplace=True)
#%% check variable names
base_names = (base.iloc[0, :].str.strip() + "_" + base.iloc[1, :].str.strip()).str.replace(" ", "_").str.replace("__", "_").str.replace("_GBP", "_$")
inflation_names = (inflation.iloc[0, :].str.strip() + "_" + inflation.iloc[1, :].str.strip()).str.replace(" ", "_").str.replace("__", "_").str.replace("_GBP", "_$")
european_names = (european.iloc[0, :].str.strip() + "_" + european.iloc[1, :].str.strip()).str.replace(" ", "_").str.replace("__", "_").str.replace("_GBP", "_$")
(inflation_names == european_names).all() #True, which means that inflation and european have the same variable names
all([(bname == inflation_names).any() for bname in base_names]) # True, which means that all variable names of base are in inflation
#%% drop columns
drop_columns = set(european.columns) - set(keep_columns)
inflation.drop(drop_columns, axis=1, inplace=True)
european.drop(drop_columns, axis=1, inplace=True)
base_names = (base.iloc[0, :].str.strip() + "_" + base.iloc[1, :].str.strip()).str.replace(" ", "_").str.replace("__", "_").str.replace("_GBP", "_$")
inflation_names = (inflation.iloc[0, :].str.strip() + "_" + inflation.iloc[1, :].str.strip()).str.replace(" ", "_").str.replace("__", "_").str.replace("_GBP", "_$")
european_names = (european.iloc[0, :].str.strip() + "_" + european.iloc[1, :].str.strip()).str.replace(" ", "_").str.replace("__", "_").str.replace("_GBP", "_$")
(inflation_names == european_names).all() #True, which means that inflation and european have the same variable names
all([(bname == inflation_names).any() for bname in base_names]) # False, as some columns are dropped from inflation and european
base_keep = [(bname == inflation_names).any() for bname in base_names]
base = base.ix[:, base_keep]
base_names = (base.iloc[0, :].str.strip() + "_" + base.iloc[1, :].str.strip()).str.replace(" ", "_").str.replace("__", "_").str.replace("_GBP", "_$")
inflation_names = (inflation.iloc[0, :].str.strip() + "_" + inflation.iloc[1, :].str.strip()).str.replace(" ", "_").str.replace("__", "_").str.replace("_GBP", "_$")
european_names = (european.iloc[0, :].str.strip() + "_" + european.iloc[1, :].str.strip()).str.replace(" ", "_").str.replace("__", "_").str.replace("_GBP", "_$")
(inflation_names == european_names).all() #True, which means that inflation and european have the same variable names
all([(bname == inflation_names).any() for bname in base_names]) # False, as some columns are dropped from inflation and european
set(european_names) - set(base_names) # diff: some city/state level variables are missing from the base scenario
#%% year and quarter information
time_1 = ccar.year_quarter(base.iloc[2:, 0], [1950, 2020])
time_1.head()
time_1.tail()
base = base.iloc[2:, 1:]
inflation = inflation.iloc[2:, 1:]
european = european.iloc[2:, 1:]
base.index = map(str, time_1.yyyyqq.values)
inflation.index = map(str, time_1.yyyyqq.values)
european.index = map(str, time_1.yyyyqq.values)
#%% read in manheim index
manheim_file = "raw/Manheim Estimates 150421 External.xlsx"
manheim_sheet = "Estimates"
manheim = pd.read_excel(manheim_file, manheim_sheet, header=3, index_col=None)
time_2 = ccar.year_quarter(pd.Series(manheim.index, index=manheim.index), [1950, 2020])
manheim.index = map(str, time_2.yyyyqq.values)
manheim_base = manheim.ix[:, ["DFAST Base"]]
manheim_base.columns = [0]
base = pd.concat([manheim_base, base], axis=1)
manheim_inflation = manheim.ix[:, ["DFAST Inflation Shock"]]
manheim_inflation.columns = [0]
inflation = pd.concat([manheim_inflation, inflation], axis=1)
manheim_european = manheim.ix[:, ["DFAST European Shock"]]
manheim_european.columns = [0]
european = pd.concat([manheim_european, european], axis=1)

#%% mapping columns/variables
inflation.columns = ["x"+str(i) for i in range(1, inflation.shape[1]+1)]
european.columns = ["x"+str(i) for i in range(1, european.shape[1]+1)]
base.columns = inflation.columns[[list(inflation_names).index(bname) for bname in base_names]]

#%%make sure base, inflation and european matches
(base.iloc[:196, ].isnull() == european.ix[:196, base.columns].isnull()).all().all()
((base.iloc[:196, ] == european.ix[:196, base.columns]) | base.iloc[:196, ].isnull()).all() # seems OK
sum(base.ix[:196, "x54"] == european.ix[:196, "x54"])
sum(base.ix[:196, "x65"] == european.ix[:196, "x65"])
sum(base.ix[:196, "x70"] == european.ix[:196, "x70"])
sum(base.ix[:196, "x71"] == european.ix[:196, "x71"])

#%% combine time
time_1.index = map(str, time_1.yyyyqq.values)
base = pd.concat([time_1, base], axis=1)
inflation = pd.concat([time_1, inflation], axis=1)
european = pd.concat([time_1, european], axis=1)

#%% write data into files
base.to_csv("clean/dfast_base.csv", index=False)
inflation.to_csv("clean/dfast_inflation.csv", index=False)
european.to_csv("clean/dfast_european.csv", index=False)
