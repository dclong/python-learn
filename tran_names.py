# -*- coding: utf-8 -*-
"""
Created on Tue May 19 09:02:08 2015

@author: bdu01
"""


def lag_names(name):
    lags = ["", "_l1", "_l2", "_l3"]
    return [name+lag for lag in lags]
#end def

def tran_names(name):
    if type(name) == list:
        return [tn for n in name for tn in tran_names(n)]
    #end if
    tnames = lag_names(name)
    tnames.extend(lag_names(name + '_qqc'))
    tnames.extend(lag_names(name + '_qqg'))
    tnames.extend(lag_names(name + '_yyc'))
    tnames.extend(lag_names(name + '_yyg'))
    return tnames
#end def

