# -*- coding: utf-8 -*-
"""
Created on Tue May 19 13:56:51 2015

@author: Ben Chuanlong Du
"""

import os
os.chdir("C:/Users/bdu01/Downloads/archives/projects/ia_lgd/ben_2/code/tran_names")
import tran_names as tn
tn = reload(tn)

predictors = ["ur_hybrid", "gdp_hybrid", "x32", "x42", "x17", "x19", "x20", "x1", "hpi_hybrid"]
names = tn.tran_names(predictors)
print(" ".join(names))

# unemployment rate related variables
ur = ["ur_hybrid", "x65", "x64", "x3"]
names = tn.tran_names(ur)
print(" ".join(names))



### Cluster C: Interest Rate Related
interest_rates = ["x32", "x42", "x17", "x19", "x20"]
names = tn.tran_names(interest_rates)
print(" ".join(names))

# HPI related variables
hpi = ["hpi_hybrid", "x58", "x60", "x57"]
hpi_names = tn.tran_names(hpi)
print(" ".join(hpi_names))
hpi_yyc_qqc = [e for e in hpi_names if re.search("yyc|qqc", e)]
print(" ".join(hpi_yyc_qqc))

# GDP related variables
gdp = ["gdp_hybrid", "x69", "x68", "x2"]
gdp_names = tn.tran_names(gdp)
print(" ".join(gdp_names))
gdp_yyg_qqg = [e for e in gdp_names if re.search("yyg|qqg", e)]
print(" ".join(gdp_yyg_qqg))

# Manheim Index related
mi = "x1"
names = tn.tran_names(mi)
print(" ".join(names))
