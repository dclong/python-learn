# -*- coding: utf-8 -*-
"""
Created on Thu May 28 03:03:42 2015

@author: Ben Chuanlong Du
"""
import pandas as pd
import re

# get year information. Split yy:q and recover year information

# @priority 2
def filter_columns(main_headers, sub_headers, drop_pattern="%"):
    """Keep only useful columns.
    Basically for each main header group, % values are dropped, 
    i.e., only the original variable is keep. 
    % and other transformations will be created later.

    1. Allow selecting columns manually.
    2. General rules to filter columns.
    3. Allow adding columns back.

    @param main_headers @todo
    @param sub_headers @todo
    @return: @todo

    """
    pass 
#end def 

def fill_merged_cells(df, rows):
    """Fill up missing headers.

    @param headers @todo
    @return: @todo

    """
    if type(rows) == list:
        for row in rows:
            fill_merged_cells(df, row)
        #end for
    #end if
    ncol = df.shape[1]
    for i in range(1, ncol):
        if pd.isnull(df.iloc[rows, i]):
            df.iloc[rows, i] =df.iloc[rows, i-1]
        #end if
    #end for
#end def 

def year_quarter(time, year_range):
    """Recover year and quarter information.

    @param quarter a column of yy:q, yyyy:q, yyQq, yyyyQq, etc.
    @return: @todo

    """
    # get rid of white spaces and then split by ":"
    time = time.str.replace("\s", "").str.split(":")
    #end if
    # convert to integers
    # time = time.apply(lambda x : [int(x[0]), int(x[1])])
    # recover 
    year = [recover_year(int(t[0]), year_range=year_range) for t in time]
    quarter = [int(t[1]) for t in time]
    yyyyqq = [e[0] * 100 + e[1] for e in zip(year, quarter)]
    # yyqq = [str(e[0])[2:] + "Q" + str(e[1]) for e in zip(year, quarter)]
    # snapshot month
    snapshot_month = [e[0]*100 + (e[1]-1)*3 if e[1] > 1 else (e[0]-1)*100 + 12 for e in zip(year, quarter)]
    return pd.DataFrame({"year":year, "quarter":quarter, "yyyyqq":yyyyqq, "snapshot_month":snapshot_month}, index=time.index)
#end def 
    
def recover_year(year, year_range=None):
    if type(year) == list:
        # @TODO: use element order to help you recover year information ...
        # not sure whether this is a good idea ...
        pass
    #end if
    lower_limit = year_range[0]
    upper_limit = year_range[1]
    if lower_limit <= year <= upper_limit:
        return year
    #end if
    year_plus_1900 = year + 1900
    if lower_limit <= year_plus_1900 <= upper_limit:
        return year_plus_1900
    #end if
    year_plus_2000 = year + 2000
    if lower_limit <= year_plus_2000 <= upper_limit:
        return year_plus_2000
    #end if
    return year
#end def

