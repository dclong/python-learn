# -*- coding: utf-8 -*-
"""
Created on Mon Jun 01 09:19:18 2015

@author: bdu01
"""
#%%
import itertools as it
import csv
import os
os.chdir("C:\\Users\\bdu01\\Downloads\\archives\\projects\\ia_lgd\\ben_2\\code\\feature_selection")
#%%
unemployment_rate = """
x3_qqg
ur_hybrid_qqg
x3_qqc
x65_qqg
ur_hybrid_qqc
x65_qqc
x3_qqg_l1
x65_qqg_l1
ur_hybrid_qqg_l1
x3_qqc_l1
x3_yyg
x65_yyg
ur_hybrid_qqc_l1
ur_hybrid_yyg
x65_qqc_l1
x3_yyc
ur_hybrid_yyc
x65_yyc
x3_qqg_l2
ur_hybrid_qqg_l2
"""
unemployment_rate = """
ur_hybrid_qqg
ur_hybrid_qqc
ur_hybrid_qqg_l1
ur_hybrid_qqc_l1
ur_hybrid_yyg
ur_hybrid_yyc
ur_hybrid_qqg_l2
ur_hybrid_qqc_l2
ur_hybrid_yyg_l1
ur_hybrid_yyc_l1
ur_hybrid_qqg_l3
ur_hybrid_yyg_l2
ur_hybrid_qqc_l3
ur_hybrid_yyc_l2
ur_hybrid_yyg_l3
ur_hybrid_yyc_l3
ur_hybrid
ur_hybrid_l1
ur_hybrid_l2
ur_hybrid_l3
"""
unemployment_rate = unemployment_rate.split()
len(unemployment_rate)
#%%
interest_rate = """
x42_l3
x32_l3
x42_l2
x32_l2
x20_l3
x20_l2
x20_l1
x42_l1
x32_l1
x20
x17_l3
x42
x32
x17_l2
x17_l1
x17
x19_l3
x19
x19_l2
x19_l1
"""
interest_rate = interest_rate.split()
len(interest_rate)
#%%
hpi = """
x57_qqc
hpi_hybrid_qqc
x58_qqc
x57_qqc_l1
x57_qqg
hpi_hybrid_qqc_l1
hpi_hybrid_qqg
x57_yyc
x57_qqg_l1
x58_qqg
hpi_hybrid_yyc
x58_qqc_l1
x57_yyg
x57_qqc_l2
hpi_hybrid_qqg_l1
x58_yyc
x58_qqg_l1
x57_yyc_l1
x57_qqg_l2
hpi_hybrid_yyg
"""
hpi = """
hpi_hybrid_qqc
hpi_hybrid_qqc_l1
hpi_hybrid_qqg
hpi_hybrid_yyc
hpi_hybrid_qqg_l1
hpi_hybrid_yyg
hpi_hybrid_qqc_l2
hpi_hybrid_yyc_l1
hpi_hybrid_qqg_l2
hpi_hybrid_yyg_l1
hpi_hybrid_qqc_l3
hpi_hybrid_yyc_l2
hpi_hybrid_qqg_l3
hpi_hybrid_yyg_l2
hpi_hybrid_yyc_l3
hpi_hybrid_yyg_l3
"""
hpi = hpi.split()
len(hpi)
#%%
manheim = """
x1_yyc_l1
x1_yyg_l1
x1_yyc
x1_yyc_l2
x1_yyg
x1_yyg_l2
x1_yyc_l3
x1_yyg_l3
x1_qqc_l3
x1_qqg_l3
x1_qqc_l2
x1_qqg_l2
x1_qqc_l1
x1_qqg_l1
x1_qqc
x1_qqg
"""
manheim = manheim.split()
len(manheim)
#%%
llvs = it.combinations(["orig_ltv_rto_grp", "cur_bal_amt_grp", "vehicle_age_grp", "term_grp", "joint_applcnt_ind"], 3)
variable_combs = it.product(llvs, hpi, manheim, unemployment_rate)
variable_combs = [comb[0] + comb[1:] for comb in variable_combs]
variable_combs = tuple(it.product(hpi, manheim, unemployment_rate))
len(variable_combs)
llvs = list(it.combinations(["orig_ltv_rto_grp", "cur_bal_amt_grp", "vehicle_age_grp", "term_grp", "joint_applcnt_ind"], 3))
len(llvs)
print("\n".join([" ".join(e) for e in llvs]))
#%%
with open("variable_combs_3.csv", "wb") as f:
    writer = csv.writer(f)
    writer.writerows(variable_combs)
