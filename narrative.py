import os
import json
import nltk

def write_to_json(directory, json_file):
    cases = read_original_data(directory)
    json.dump(cases, open(json_file, "w"))
#end def

def read_raw_data(directory):
    cases = []
    for f in os.listdir(directory):
        case = os.path.splitext(f)[0].split("_")
        if case[1] == "Y":
            case[1] = True
        else:
            case[1] = False
        #end if
        narrative = open(os.path.join(directory, f), "r").read()
        # fix bad encodings
        reps = [
                ["\xa0", " "],
                ["\xa1", "!"],
                ["\xa2", " cent "],
                ["\xa3", " GBP "],
                ["\xa4", " GCS "],
                ["\xa5", " CRY "],
                ["\xa6", "|"],
                ["\xa7", ""],
                ["\xa8", "-"],
                ["\xa9", ""],
                ["\xaa", "a"],
                ["\xab", "<<"],
                ["\xac", "-"],
                ["\xad", ""],
                ["\xae", ""],
                ["\xaf", "-"],
                ["\xb0", "0"],
                ["\xb1", "+/-"],
                ["\xb2", "2"],
                ["\xb3", "3"],
                ["\xb4", "'"],
                ["\xb5", "u"],
                ["\xb6", ""],
                ["\xb7", "*"],
                ["\xb8", ","],
                ["\xb9", "1"],
                ["\xba", "0"],
                ["\xbb", ">>"],
                ["\xbc", "1/4"],
                ["\xbd", "1/2"],
                ["\xbe", "3/4"],
                ["\xbf", "?"],
                ["\xc0", "A"],
                ["\xc1", "A"],
                ["\xc2", "A"],
                ["\xc3", "A"],
                ["\xc4", "A"],
                ["\xc5", "A"],
                ["\xc6", "AE"],
                ["\xc7", "C"],
                ["\xc8", "E"],
                ["\xc9", "E"],
                ["\xca", "E"],
                ["\xcb", "E"],
                ["\xcc", "I"],
                ["\xcd", "I"],
                ["\xce", "I"],
                ["\xcf", "I"],
                ["\xd0", "D"],
                ["\xd1", "N"],
                ["\xd2", "O"],
                ["\xd3", "O"],
                ["\xd4", "O"],
                ["\xd5", "O"],
                ["\xd6", "O"],
                ["\xd7", "*"],
                ["\xd8", "O"],
                ["\xd9", "U"],
                ["\xda", "U"],
                ["\xdb", "U"],
                ["\xdc", "U"],
                ["\xdd", "Y"],
                ["\xde", "th"],
                ["\xdf", "s"],
                ["\xe0", "a"],
                ["\xe1", "a"],
                ["\xe2", "a"],
                ["\xe3", "a"],
                ["\xe4", "a"],
                ["\xe5", "a"],
                ["\xe6", "ae"],
                ["\xe7", "c"],
                ["\xe8", "e"],
                ["\xe9", "e"],
                ["\xea", "e"],
                ["\xeb", "e"],
                ["\xec", "i"],
                ["\xed", "i"],
                ["\xee", "i"],
                ["\xef", "i"],
                ["\xf0", "eth"],
                ["\xf1", "n"],
                ["\xf2", "o"],
                ["\xf3", "o"],
                ["\xf4", "o"],
                ["\xf5", "o"],
                ["\xf6", "o"],
                ["\xf7", "/"],
                ["\xf8", "o"],
                ["\xf9", "u"],
                ["\xfa", "u"],
                ["\xfb", "u"],
                ["\xfc", "u"],
                ["\xfd", "y"],
                ["\xfe", "th"],
                ["\xff", "y"],
            ]
        for rep in reps:
            narrative = narrative.replace(rep[0], rep[1])
        #end for
        case.append(narrative)
        cases.append(case)
    #end for
    return cases
#end def

def find_str(cases, obj):
    for i in range(len(cases)):
        case = cases[i]
        narrative = case[2]
        index = narrative.find(obj)
        if index >= 0:
            case.append(content(narrative, index))
            return case
        #end if
    #end for
    return None
#end def

def content(narrative, position):
    begin_index = backward(narrative, position, " ", 5)
    end_index = forward(narrative, position, " ", 5)
    return narrative[begin_index:end_index]
#end def

def backward(narrative, position, delimiter, count):
    n = 0
    start_index = position - 1
    while start_index > 0:
        if narrative[start_index] == delimiter:
            if n == count:
                return start_index + 1
            else:
                n += 1
            #end if
        #end if
        start_index -= 1
    #end while
    return 0
#end def

def forward(narrative, position, delimiter, count):
    size = len(narrative)
    n = 0
    end_index = position + 1
    while end_index < size:
        if narrative[end_index] == delimiter:
            if n == count:
                return end_index
            else:
                n += 1
            #end if
        #end if
        end_index += 1
    #end while
    return size
#end def

def dist_freq(narrs, n):
    fd = nltk.FreqDist()
    porter = nltk.PorterStemmer()
    for narr in narrs:
        items = nltk.util.ngrams([porter.stem(t.lower()) for t in nltk.word_tokenize(narr)], n)
        fd.update(items)
    #end for
    return fd
#end def

def drop_meaningless(df):
    meaningless = [
        ".", ":", "$", ".", ",", 
        "a", 
        "an", "as", "be", "is", "on", "by", "or", "up", 
        "in", "to", "of", 
        "and", "the", "for", "are", 
        "that", "also", 
        "with", 
        "which", 
        "between", 
    ]
    for m in meaningless:
        
#end def