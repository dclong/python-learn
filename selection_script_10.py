# -*- coding: utf-8 -*-
"""
Created on Mon Jun 01 15:17:54 2015

@author: bdu01
"""
#%% suffix
suffix = "10"
#%%
import pandas as pd
import numpy as np
import os
os.chdir("C:\\Users\\bdu01\\Downloads\\archives\\projects\\ia_lgd\\ben_2\\code\\feature_selection\\08")
os.getcwd()
import selection as st
reload(st)
#%%
pe = pd.read_csv(suffix + "\\parameter_estimates_" + suffix + ".csv")
pe.head(20)
#%%
# add a model_id column
num_param = 11
pe['model_id'] = np.repeat(range(pe.shape[0]/num_param), num_param)
# concatenate Variable and ClassVal0
pe['Variable'] = [pe.iloc[i,0] if pd.isnull(pe.iloc[i,1]) else pe.iloc[i,0] + "_" + str(pe.iloc[i,1]) for i in range(pe.shape[0])]
# the ClassVal0 column is of no use now
pe.drop('ClassVal0', axis=1, inplace=True)
# the DF column is all 1 and is thus not useful
pe.drop('DF', axis=1, inplace=True)
# get rid of "<" in ProbChiSq and cast to numbers
set(map(type ,pe.ix[:, "ProbChiSq"]))
pe["ProbChiSq"] = pe.ix[:, "ProbChiSq"].str.replace("<", "").map(float)
#%%
# convert integer index to string to avoid confusing problems
pe.index = ["r" + str(i) for i in pe.index]
good_models = [st.filter_model(pe.ix[pe.model_id==i, ]) for i in range(pe.model_id.max()+1)]
good_models = [m for m in good_models if m]
good_models = pd.DataFrame(good_models)
good_models.to_excel(suffix + "\good_models_" + suffix + ".xlsx")
