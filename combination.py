# -*- coding: utf-8 -*-
"""
Created on Tue May 19 13:56:51 2015

@author: Ben Chuanlong Du
"""
from __future__ import division
import re
import os
os.chdir("C:/Users/bdu01/Downloads/archives/projects/ia_lgd/ben_2/code/tran_names")
import tran_names as tn

# unemployment rate related variables
# don't use WI as suggested by Mike
ur = ["ur_hybrid", "x65", "x3"]
ur = tn.tran_names(ur)
print(" ".join(ur))
len(ur)



### Cluster C: Interest Rate Related
# don't use growth versions
it = ["x32", "x42", "x17", "x19", "x20"]
it = tn.tran_names(it)
it = [e for e in it if not re.search("yyg|qqg", e)]
print(" ".join(it))
len(it)

# HPI related variables
# drop Milwaukee HPI and level ones
hpi = ["hpi_hybrid", "x58", "x57"]
hpi = tn.tran_names(hpi)
hpi_yyc_qqc = [e for e in hpi if re.search("yyc|qqc|yyg|qqg", e)]
print(" ".join(hpi))
len(hpi)

# GDP related variables
# drop WI GDP and level ones
gdp = ["gdp_hybrid", "x69", "x2"]
gdp = tn.tran_names(gdp)
gdp = [e for e in gdp if re.search("yyc|qqc|yyg|qqg", e)]
print(" ".join(gdp))
len(gdp)

# Manheim Index related
# drop level ones
mi = "x1"
mi = tn.tran_names(mi)
mi = [e for e in mi if re.search("yyc|qqc|yyg|qqg", e)]
print(" ".join(mi))
len(mi)
